#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
using namespace std;

class Date
{
private:
    int day;
    int month;
    int year;
public:
    Date(int d, int m, int y);
    void set_date(int d, int m, int y);
    void print_day();

    int get_day() {return day;}
    int get_month() {return month;}
    int get_year() {return year;}
};

Date::Date(int d, int m, int y)
{
    set_date(d, m, y);
}

bool is_leap_year(int year)
{
    int r = year % 33;
    return r == 1 || r == 5 || r == 9 || r == 13 || r == 17 || r == 22 || r == 26 || r == 30;
}

int days_of_month(int m , int y)
{
    if (m < 7)
        return 31;
    else if (m < 12)
        return 30;
    else if (m == 12)
        return is_leap_year(y) ? 30 : 29;
    else  
        abort();    
}

void Date::set_date(int d, int m, int y)
{
    if (y < 0 || m < 1 || m > 12 || d < 1 || d > days_of_month(m, y))
    {
        abort();
    }
    day = d;
    month = m;
    year = y;
}

void Date::print_day()
{
    cout << day << "/" << month << "/" << year;
}


class person
{
private:
    string name;
    Date bdate;
public:
    person(string s, int d, int m, int y);
    Date get_bdate() {return bdate;}
    string get_name() {return name;}
};

person::person(string s , int d , int m, int y)
    :bdate (d , m , y)
{
    if (s == "")
    {
        abort();
    }
    name = s;
}


int main()
{
    vector<person> vp; 
    vp.push_back(person("hassan", 6, 5, 1380));
    
    cout << vp[0].get_name() << endl;
    Date bd (6, 5, 1380);
    bd.print_day();
    cout << '\n';
    cout << bd.get_day() << endl;

    return 0;
}